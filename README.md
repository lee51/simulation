simulation
------------

Created:  2014-12-15 (Ed Lee)

Simulation tool for randomly generating data "measured"
by clustered devices (e.g., probes).

Requires a Solr server (http://lucene.apache.org/solr)
to store the following parameters:

  * general (type default_params): required
  * cluster (type cluster_params): optional
  * device (type device_params): optional

Default and other parameters can be set using the
set_params.py script.

Before running simulation.py, you must first:

  * install a Solr server
  * register an indexing schema (schema.xml)
  * change the SOLR_URL parameter according to the Solr host
  * install python dependencies, using `pip -r requirements.txt`
  * enter default parameters, using set_params.py
  * enter cluster or device parameters (optional)
