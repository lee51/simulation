import collections
import gevent
import hashlib
import logging
import requests

from gevent.queue import Queue

#======================================================================

class Bear(gevent.Greenlet):

    def __init__(self, hibernation=10):
        self._hibernation = hibernation # seconds
        gevent.Greenlet.__init__(self)

    def forage(self):
        """
        Look for food
        """
        raise NotImplemented()

    def _run(self):
        self.running = True
        self.cycles = 0

        while self.running:
            self.forage()
            self.cycles += 1
            gevent.sleep(self._hibernation)

#======================================================================

class Actor(gevent.Greenlet):

    def __init__(self):
        self.inbox = Queue()
        gevent.Greenlet.__init__(self)

    def receive(self, message):
        """
        Define in your subclass.
        """
        raise NotImplemented()

    def send(self, message):
        self.inbox.put(message)

    def _run(self):
        self.running = True

        while self.running:
            message = self.inbox.get()
            self.receive(message)

#======================================================================

class Deque(collections.deque):

    def __init__(self, maxlen=None):
        self.__maxlen = maxlen
        super(self.__class__, self).__init__()

    def push(self, item):
        self.append(item)
        if self.__maxlen:
            for _ in xrange(len(self)-self.__maxlen): self.popleft()

#======================================================================

def generate_md5(*args):
    """
    Creates md5 (128-bit) hash from string arguments
    """

    # Create hash
    m = hashlib.md5()

    # If no arguments provided, use current timestamp (UTC in seconds)
    if not args:
        t = datetime.utcnow()
        args = (t.strftime("%s"),)

    # Update hash with args
    for arg in args:
        m.update(arg)

    # Return 128-bit (16-byte) md5 hash value (32-digit hex)
    return m.hexdigest()

#======================================================================

logging.getLogger("requests").setLevel(logging.WARNING)
http_headers = {'Content-Type': 'application/json'}

def get(url, query={}, baseURL=None):
    # Force response to be json, not XML
    if "wt" not in query: query["wt"] = "json"
    # Append baseURL to url
    if baseURL: url = "/".join([baseURL.rstrip("/"), url.lstrip("/").rstrip("?")])
    # Return HTTP response
    return requests.get(url, params=query)

def post(url, data, baseURL=None):
    # Append baseURL to url
    if baseURL: url = "/".join([baseURL.rstrip("/"), url.lstrip("/")])
    # Return HTTP response
    return requests.post(url, headers=http_headers, data=data)
