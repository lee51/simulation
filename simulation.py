#!/usr/bin/env python

"""
  simulation.py

      Simulation tool for randomly generating data "measured"
      by clustered devices (e.g., probes).

      Requires a Solr server (http://lucene.apache.org/solr)
      to store the following parameters:

        * general (type default_params): required
        * cluster (type cluster_params): optional
        * device (type device_params): optional

      NOTA BENE!
      Default parameters must first be set using set_params.py!
      You can choose to post data to Solr by changing the
      callback function in generate_data() from None to write_data
"""

# Import libraries (may require install)
import gevent
import json
import logging
import os
import matplotlib.pyplot as plt
import random
import string
import time

from datetime import datetime, timedelta
from gevent.queue import Queue
from gevent.pool import Pool
from gevent.monkey import patch_all

# Local modules
from settings import Settings
from utils import Actor, Bear, Deque, get, post, generate_md5

#======================================================================
# Parameters
#======================================================================
CYCLES = 2000    # Number of cycles to run (-1 for infinite loop)
DSIZE = 2000     # Length of data deque (memory limited)
PERIOD = 0       # Seconds between "measurements"
SOLR_URL = "http://localhost:8983/solr/datasim"
FIELDS = ["field1", "field2"] # e.g., temperature and salinity

# Other constants
GENERAL = "general"
GEN_TYPE = "default_params"
CLUSTER_FMT = "Cluster_%d"
CLUSTER_TYPE = "cluster_params"
DEV_TYPE = "device_params"
SEED = 1 # Random seed for device ID generation

# Logging level is set in initialize()
logger = logging.getLogger()

#======================================================================

def main():

    # Initialization returns general settings
    general = initialize()

    # Generate device clusters (dict)
    clusters = create_clusters(5, inherits=general)

    # Fetch remote data and update local params (clusters)
    refresh(clusters, CLUSTER_TYPE)

    # Generate or read list of devices (dict)
    devices = generate_devices(25, clusters)

    # Fetch remote data and update local params (devices)
    refresh(devices, DEV_TYPE)

    # Store (id, version) pairs in a dictionary
    versions = retrieve_versions(general)
    for _c in clusters.itervalues():
        versions.update(retrieve_versions(_c))
    for _d in devices.itervalues():
        versions.update(retrieve_versions(_d))

    # Create update Actor
    updater = Update()
    updater.load_data(general, clusters, devices)
    updater.start()

    # Create poller
    poller = Poller(5) # Sleep 5 sec after feeding
    poller.load_data(versions, updater)
    poller.start()

    # Create greenlet Pool of workers
    worker_pool = Pool(len(devices))

    # Hold time series data in dict of dicts of deques
    data = {dev: {field: Deque(DSIZE) for field in FIELDS} for dev in devices}

    # Set up loop meta-variables
    tmin = None # Will be given a value from first iteration
    global count
    count = 0

    TIC = time.time()
    logger.info("Starting loop...")

    while count != CYCLES:

        # The function time.time() returns the current system time
        # in ticks since 12:00am, January 1, 1970 (epoch)
        tic = time.time()

        # The variable 'now' contains a formatted datetime to be used
        # for a different purpose than tic/toc
        now = datetime.utcnow()
        if CYCLES<10: logger.info(now)

        # Variables 'tmin' and 'tmax' are simply placeholders for plot range
        if not tmin: tmin = now
        tmax = now

        # Iterate on fields (power, voltage)
        for field in FIELDS:

            def job(params):
                # Grab pointer to relevant data
                p = params.get_field(field)
                d = data[params.name][field]
                generate_data(now, p, d, count=count, callback=None)

            # Iterate on devices
            worker_pool.map(job, devices.values())

        count += 1

        worker_pool.join()
        gevent.sleep(PERIOD-(time.time()-tic+0.0013))

    logger.info("Loop execution time: %.3f s (%d iterations, %d devices)" % \
        (time.time()-TIC, count, len(devices)))

    poller.running = False
    updater.running =False

    if CYCLES>3:
        for d in devices.values():
            t, v, dv = zip(*data[d.name][FIELDS[0]])
            plt.plot(t, v)
            plt.ylim(0, 2)
        plt.show()

#======================================================================

class Poller(Bear):
    def load_data(self, versions, updater):
        self.versions = versions
        self.updater = updater

    def forage(self):
        # Retrieve all remotely persisted parameters (all types, all fields)
        r = get("select", query={"q":"type:*_params"}, baseURL=SOLR_URL)
        if r.status_code is 200:
            response = json.loads(r.text)["response"]
        else: raise IOError, "Could not refresh settings from Solr: %s" % r.text

        for i in xrange(response["numFound"]):
            entry = response["docs"][i]
            if entry["_version_"] not in self.versions: self.updater.send(entry)

#======================================================================

class Update(Actor):
    def load_data(self, general, clusters, devices):
        self.general = general
        self.clusters = clusters
        self.devices = devices

    def receive(self, p):
        # Update parameters
            if p["type"] == GEN_TYPE:
                _newkeys = self.general.reconcile(p)
                for _m in self.devices.values(): _m.reconcile(p)
            elif p["type"] == CLUSTER_TYPE:
                _cluster = self.clusters[p["name"]]
                _newkeys = _cluster.reconcile(p)
                for _ in _cluster.children: self.devices[_].reconcile(p)
            elif p["type"] == DEV_TYPE:
                self.devices[p["name"]].reconcile(p)
            else:
                raise TypeError, "Parameter update not supported for %s" % p["type"]

            if _newkeys:
                logger.info("Updated keys %s for %s" % (_newkeys, p["name"]))

#======================================================================

def initialize():

    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    settings = Settings(GENERAL, type=GEN_TYPE, fields=FIELDS)

    for field in FIELDS:
        # Check for general settings (for each field)
        qid = generate_md5(GENERAL, GEN_TYPE, field)
        r = get("select", query={"q":"id:%s" % qid}, baseURL=SOLR_URL)
        if r.status_code is 200:
            response = json.loads(r.text)["response"]
        else: raise IOError, "Could not retrieve general settings from Solr: %s" % r.text

        # If found, use values from db
        if response["numFound"]>1: # Implies ERROR
            raise IOError, "More than one setting found!\n%s" % response["docs"]

        elif response["numFound"]==1: # Most favorable situation
            entry = response["docs"][0]
            version = entry.get("_version_", None)
            logger.info("%s settings version %s found for field '%s'" % (GENERAL, version, field))
            newkeys = settings.reconcile(entry)

        else:
            logger.info("No records founds for field '%s'" % field)
            raise KeyError, "No general settings could be found for field '%s'" % field

    return settings

#======================================================================

def generate_devices(n, clusters):
    # Generate list of devices
    devList = dict()
    random.seed(SEED)
    for _ in xrange(n):
        name = ''.join(random.choice(string.ascii_uppercase + string.digits) \
            for _ in xrange(8))
        c = random.choice(clusters.values())
        devList[name] = Settings(name, type=DEV_TYPE, inherits=c)
        c.children.add(name)
    return devList

#======================================================================

def read_devices(filename, clusters):
    devList = dict()
    with open(filename, "r") as f:
        headers = f.readline().strip("\n")
        for line in f:
            name, c_suffix = line.strip("\n").split(",")
            # Append cluster prefix to cluster number
            c_name = "_".join(("Cluster", c_suffix))
            dev = Settings(name, type=DEV_TYPE, inherits=clusters[c_name])
            dev.update("attributes", cluster=c_name)
            devList[dev.name] = dev
            clusters[c_name].children.add(dev.name)
    return devList

#======================================================================

def create_clusters(number, inherits=None):
    """
    Creates clusters (Cluster_1, Cluster_2, ..., Cluster_N)
    """
    clusterList = dict()
    for i in xrange(1, number+1):
        cluster = Settings(CLUSTER_FMT % i, type=CLUSTER_TYPE, inherits=inherits)
        clusterList[cluster.name] = cluster
    return clusterList

#======================================================================

def refresh(these, ptype):
    """
    Updates local information with remote data
    """

    # Check for general settings (for all fields)
    r = get("select", query={"q":"type:%s" % ptype}, baseURL=SOLR_URL)
    if r.status_code is 200:
        response = json.loads(r.text)["response"]
    else: raise IOError, "Could not refresh settings from Solr: %s" % r.text

    for i in xrange(response["numFound"]):
        entry = response["docs"][i]
        name = entry["name"]
        field = entry["field"]
        logger.info("%s settings version %s found for field '%s'" % (ptype, version, field))
        # Variable 'these' should be a dictionary of settings
        these[name].reconcile(entry)

#======================================================================

def retrieve_versions(param):
    versions = set()
    for field in FIELDS:
        p = param.get_field(field)
        versions.add(p["_version_"])
    return versions

#======================================================================

def generate_data(t, p, d, count=0, callback=None):

    shift = p["abs_shift"]
    if shift:
        p["central_tend"] += shift
        p["upper_bound"] += shift
        p["lower_bound"] += shift
        p["abs_shift"] = 0  # NB: 'shift' is not set to 0!

    if count: # Not first time (v and dv defined)

        prev, dv = d[-1][1:]
        prev += shift
        force = (p["central_tend"] - prev)**3

        # Introduce stochastic jumping
        toss = random.normalvariate(0, 1)

        # "Extreme" events (e.g., 3 => 1%)
        if abs(toss) > p["extreme_sigma"]:
            dv = p["max_delta"] * toss + p["centering"] * force

        # Quiet periods (e.g., 3 => 99%)
        else:
          
            # Reset dv
            if abs(dv) > p["max_delta"]:
                dv *= (1.0-p["braking"])         # decelerate a lot

            elif abs(dv) > 0.1*p["max_delta"]:
                dv *= (1.0-p["dragging"])        # decelerate a bit

            elif abs(dv) > 0.01*p["max_delta"]:
                dv *= -(1.0-p["dragging"])       # decelerate and reverse

            dv += p["oscillation"] * toss        # add noise

        # Compute new value
        value = prev + dv

        # If new value exceeds bounds, reverse direction
        if value < p["lower_bound"] or value > p["upper_bound"]:
            dv *= -0.1  # reverse
            value = prev + dv

        # If still exceeds bounds, reset to initial value.
        if value < p["lower_bound"] or value > p["upper_bound"]:
            value = d[0][1] + shift
            dv = 0.

    else: # Set initial value
        vrange = p["upper_bound"] - p["lower_bound"]
        value = p["central_tend"] + random.normalvariate(0, p["max_delta"]*vrange)
        if value<p["lower_bound"] or value>p["upper_bound"]: value = p["central_tend"]
        dv = 0

    #return value, dv
    new = (t, value, dv)
    if callback: callback(p, new)
    d.push(new)

#======================================================================

def write_data(params, data):

    message = {"type": "device_data"}
    message["name"] = params["name"]
    message["field"] = params["field"]
    message["time"] = data[0].isoformat()[:-7]+"Z"
    message["value"] = data[1]
    message["id"] = generate_md5(message["name"],
                                 message["type"],
                                 message["field"],
                                 message["time"])
    r = post("update/json", json.dumps([message]), baseURL=SOLR_URL)
    if r.status_code!=200:
        print r.text
        exit()
    
#======================================================================

if __name__=='__main__':
    patch_all()
    try: main()
    except KeyboardInterrupt: exit()
