import json
from settings import Settings, generate_md5

class Test_Settings:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_init1(self):
        s = Settings("something", "something_type")
        assert s.name is "something"
        assert s.type is "something_type"
        assert s.fields == set()
        attr = s.get_field("attributes")
        assert attr["name"] is "something"
        assert attr["type"] is "something_type"

    def test_init2(self):
        s1 = Settings("s1", "something_type", fields=["pink", "floyd"])
        assert len(s1.fields) is 2
        assert len(s1._data) is 3

        s2 = Settings("s2", "something_else", inherits=s1)
        assert len(s2.fields) is 2
        md5a = generate_md5("s2", "something_else", "pink")
        assert s2.get_field("pink")["id"] == md5a
        md5b = generate_md5("s2", "something_else", "floyd")
        assert s2.get_field("floyd")["id"] == md5b

    def test_json(self):
        s3 = Settings("s3", "something_type", fields=["jobim", "regina"])
        s3.update("jobim", firstname="tom")
        s3.update("regina", firstname="elis")
        # Take first element of json because wrapped in []
        assert json.loads(s3.json("jobim"))[0]["firstname"] == "tom"
        assert json.loads(s3.json("regina"))[0]["firstname"] == "elis"
        assert json.loads(s3.json("attributes"))[0]["name"] == "s3"