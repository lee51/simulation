import json
from utils import generate_md5

class Settings(object):

    def __init__(self, name, type, fields=[], inherits=None):
        self.name = name
        self.type = type
        self.fields = set()
        self.children = set()
        self._data = {"attributes": {"name": name, "type": type}}

        if fields:
            self.add_fields(*fields)

        if inherits:
            for f in inherits.fields:
                self.update(f, inherits.get_field(f))
            self.add_fields(*inherits.fields) # Actually updates rather than adding

    def __repr__(self): return "<Settings object '%s'>" % self.name
    #def __repr__(self): return str(self.get_field("attributes"))

    def add_fields(self, *fields):
        for fieldname in fields:
            newdata = {
                "id": self.md5(fieldname),
                "name": self.name,
                "type": self.type,
                "field": fieldname
            }
            if fieldname in self.fields:
                self.update(fieldname, newdata)
            else:
                self.fields.add(fieldname)
                self._data[fieldname] = newdata

    def reconcile(self, remote):
        """
        Compares two sets of parameters (dict) for a given field.
        """
        fieldname = remote["field"]
        local = self.get_field(fieldname)
        if not local: local = remote

        # setdiff = local.viewitems() ^ remote.viewitems()
        setdiff = dict(remote.viewitems() - local.viewitems())
        self.update(fieldname, setdiff)
        return setdiff.keys()

    def get_field(self, fieldname):
        """
        Each object of this class contains a '_data' member (dict), which contains
        another dict of settings for various fields (e.g., temperature, salinity).
        This method return the settings (dict) for a particular field.
        """
        return self._data.get(fieldname)

    def json(self, field, select=None):
        """
        Returns settings as JSON.  If 'select' defined, return subset as JSON.
        """
        params = self.get_field(field)
        if select:
            subset = {k: params[k] for k in params if k in select}
            return json.dumps([subset])
        else:
            return json.dumps([params])

    def md5(self, field):
        return generate_md5(self.name, self.type, field)

    def update(self, field, dictargs=None, **kwargs):
        self.fields.add(field)
        if field not in self._data:
            self._data[field] = dict()
        if dictargs:
            for key, value in dictargs.iteritems():
                self._data[field][key] = value
        if kwargs:
            for key, value in kwargs.iteritems():
                self._data[field][key] = value