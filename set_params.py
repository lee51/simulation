#!/usr/bin/env python

"""
  set_params.py

      Set parameters for default, cluster, or device on Solr server

      Note:  At least the default params ("general") must be set
             before the simulation can be run!
"""

import json
import sys

from simulation import FIELDS, SOLR_URL
from settings import Settings
from utils import post

NAME = "general"
TYPE = "default_params"

def update(settings):
    # DEFAULT SETTINGS ("general", type="default_params")
    lower_bound = {"field1": 0, "field2": 10}
    central_tend = {"field1": 1, "field2": 20}
    upper_bound = {"field1": 2, "field2": 30}
    max_delta = {"field1": 0.001, "field2": 0.01}
    extreme_sigma = {"field1": 2.8, "field2": 2.8}
    braking = {"field1": 0.9, "field2": 0.9}
    dragging = {"field1": 0.7, "field2": 0.7}
    oscillation = {"field1": 0.005, "field2": 0.005}
    centering = {"field1": 0.5, "field2": 0.5}
    abs_shift = {"field1": 0, "field2": 0}

    for field in FIELDS:
        settings.update(field, lower_bound=lower_bound[field],
                               central_tend=central_tend[field],
                               upper_bound=upper_bound[field],
                               max_delta=max_delta[field],
                               extreme_sigma=extreme_sigma[field],
                               braking=braking[field],
                               dragging=dragging[field],
                               oscillation=oscillation[field],
                               centering=centering[field],
                               abs_shift=abs_shift[field])
        rp = post("update/json", settings.json(field), baseURL=SOLR_URL)
        if rp.status_code is 200:
            response = json.loads(rp.text)
            print response["responseHeader"]
        else: raise IOError("Could not post general settings: %s" % rp.text)

def delete(query):
    command = {"delete": {"query": query}}
    rp = post("update", json.dumps(command), baseURL=SOLR_URL)
    print "Status %d: %s" % (rp.status_code, rp.text)

if __name__=='__main__':

    if len(sys.argv)>2 and sys.argv[1]=="delete":
        delete(sys.argv[2])
    elif len(sys.argv)>1 and sys.argv[1]=="delete":
        print "Please specify a delete query."
    else:
        settings = Settings(NAME, type=TYPE, fields=FIELDS)
        update(settings)